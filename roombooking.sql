-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Oct 26, 2022 at 09:02 PM
-- Server version: 5.7.36
-- PHP Version: 7.4.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `roombooking`
--

-- --------------------------------------------------------

--
-- Table structure for table `equipements`
--

DROP TABLE IF EXISTS `equipements`;
CREATE TABLE IF NOT EXISTS `equipements` (
  `id_equipement` bigint(20) NOT NULL AUTO_INCREMENT,
  `nom_equipement` varchar(255) DEFAULT NULL,
  `salle_id_salle` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_equipement`),
  KEY `FK3yc899h852mfbfc4jt2t8v3fg` (`salle_id_salle`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `hibernate_sequence`
--

DROP TABLE IF EXISTS `hibernate_sequence`;
CREATE TABLE IF NOT EXISTS `hibernate_sequence` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hibernate_sequence`
--

INSERT INTO `hibernate_sequence` (`next_val`) VALUES
(25);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
CREATE TABLE IF NOT EXISTS `products` (
  `id_product` bigint(20) NOT NULL AUTO_INCREMENT,
  `designation` varchar(255) DEFAULT NULL,
  `price` double NOT NULL,
  PRIMARY KEY (`id_product`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `reservation`
--

DROP TABLE IF EXISTS `reservation`;
CREATE TABLE IF NOT EXISTS `reservation` (
  `id_reservation` bigint(20) NOT NULL AUTO_INCREMENT,
  `date_reservation` date DEFAULT NULL,
  `heure_debut` datetime(6) DEFAULT NULL,
  `heure_fin` datetime(6) DEFAULT NULL,
  PRIMARY KEY (`id_reservation`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reservation`
--

INSERT INTO `reservation` (`id_reservation`, `date_reservation`, `heure_debut`, `heure_fin`) VALUES
(1, '2022-10-31', '2022-10-31 10:00:00.000000', '2022-10-31 12:00:00.000000'),
(2, '2022-10-31', '2022-10-31 09:00:00.000000', '2022-10-31 11:00:00.000000');

-- --------------------------------------------------------

--
-- Table structure for table `reservation_salles`
--

DROP TABLE IF EXISTS `reservation_salles`;
CREATE TABLE IF NOT EXISTS `reservation_salles` (
  `id_reservation_salles` bigint(20) NOT NULL AUTO_INCREMENT,
  `reservation_id_reservation` bigint(20) DEFAULT NULL,
  `salle_id_salle` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_reservation_salles`),
  KEY `FK6jq7judkcdxdbplnb5qemfhbc` (`reservation_id_reservation`),
  KEY `FKqy7vt8b4k7fsptyj2r56ga1lk` (`salle_id_salle`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reservation_salles`
--

INSERT INTO `reservation_salles` (`id_reservation_salles`, `reservation_id_reservation`, `salle_id_salle`) VALUES
(1, 1, 1),
(2, 2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `reservation_users`
--

DROP TABLE IF EXISTS `reservation_users`;
CREATE TABLE IF NOT EXISTS `reservation_users` (
  `id_reservation_user` bigint(20) NOT NULL AUTO_INCREMENT,
  `reservation_id_reservation` bigint(20) DEFAULT NULL,
  `user_id_user` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_reservation_user`),
  KEY `FKso30g2xp6frij5i9btwxtyioy` (`reservation_id_reservation`),
  KEY `FKdu6wcyyt17eifahjpggkggmmo` (`user_id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reservation_users`
--

INSERT INTO `reservation_users` (`id_reservation_user`, `reservation_id_reservation`, `user_id_user`) VALUES
(1, 1, 1),
(2, 2, 2),
(3, 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `salle`
--

DROP TABLE IF EXISTS `salle`;
CREATE TABLE IF NOT EXISTS `salle` (
  `id_salle` bigint(20) NOT NULL AUTO_INCREMENT,
  `capacite` int(11) DEFAULT NULL,
  `nom_salle` varchar(255) DEFAULT NULL,
  `url_picture` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_salle`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `salle`
--

INSERT INTO `salle` (`id_salle`, `capacite`, `nom_salle`, `url_picture`) VALUES
(1, 10, 'Salle X', ''),
(2, 8, 'Salle T', '');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id_user` bigint(20) NOT NULL AUTO_INCREMENT,
  `date_naissance` date DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `is_admin` bit(1) DEFAULT NULL,
  `nom` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id_user`, `date_naissance`, `email`, `is_admin`, `nom`) VALUES
(1, '1999-02-01', 'touko.doline@gmail.com', b'0', 'TOUKO'),
(2, '1999-02-01', 'doline.toukochokouafi@epfedu.fr', b'0', 'Doline'),
(3, '2000-03-01', 'fabricetouomou84@gmail.com', b'1', 'Fabrice');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `equipements`
--
ALTER TABLE `equipements`
  ADD CONSTRAINT `FK3yc899h852mfbfc4jt2t8v3fg` FOREIGN KEY (`salle_id_salle`) REFERENCES `salle` (`id_salle`);

--
-- Constraints for table `reservation_salles`
--
ALTER TABLE `reservation_salles`
  ADD CONSTRAINT `FK6jq7judkcdxdbplnb5qemfhbc` FOREIGN KEY (`reservation_id_reservation`) REFERENCES `reservation` (`id_reservation`),
  ADD CONSTRAINT `FKqy7vt8b4k7fsptyj2r56ga1lk` FOREIGN KEY (`salle_id_salle`) REFERENCES `salle` (`id_salle`);

--
-- Constraints for table `reservation_users`
--
ALTER TABLE `reservation_users`
  ADD CONSTRAINT `FKdu6wcyyt17eifahjpggkggmmo` FOREIGN KEY (`user_id_user`) REFERENCES `user` (`id_user`),
  ADD CONSTRAINT `FKso30g2xp6frij5i9btwxtyioy` FOREIGN KEY (`reservation_id_reservation`) REFERENCES `reservation` (`id_reservation`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
