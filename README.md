### Java Spring Roombooking Project

## Project Features

- Allows easy management among admin, manager and member for performing room booking. <br>
- Spring BOOT Web application with an MVC architecture (DAO - SERIVCE - CONTROLLER). <br>
- All configurations present in application.properties (including configurations for e-mails). <br>
- Pom.xml contains all dependecies we have used. <br>

## Branches

1. `master`- Contains the initial project status
2. `dev` - Branch into which all work was merged
3. All the other branches belong to individual contributors connected to `dev`


## To Run The Project

1. Open terminal. Run the following command: git clone https://gitlab.com/epf8/api.git.
2. Import project in IntelliJ IDEA making sure the file "pom.xml" is at the root of the folder.
3. go to project structure and choose Java 11 as java project version. 
4. Start and Initialize the database before running the projet; Start Docker first and when docker is done loading, copy and paste the following command (in your IntelliJ terminal or windows CMD).

```
docker run --name mariadb --rm -e MYSQL_ROOT_PASSWORD=toor -e MYSQL_DATABASE=roombooking -p 3306:3306 mariadb
```
5. Follow the necessary steps to link IntelliJ to the DB through the database tab located at the right. (Consult [III.2. Afficher la BDD dans Intellij](https://github.com/resourcepool/training-spring-boot/tree/readme-setup#2-afficher-la-bdd-dans-intellij) if need arises).
6. Launch the sql script 'roombooking.sql' located at the root of the project: Select the .sql file, Right click on the file then select 'Run'.<br>
(Consult [III.3. Initialisation de la BDD](https://github.com/resourcepool/training-spring-boot/tree/readme-setup#3-initialisation-de-la-bdd) if need arises).
7. Launch the application through IntelliJ, and verify it works ! (on http://localhost:8081 by default).<br>
   (Consult  [IV. Run du projet (c'est bientôt fini promis !)](https://github.com/resourcepool/training-spring-boot/tree/readme-setup#iv-run-du-projet-cest-bientôt-fini-promis-) if need arises).

