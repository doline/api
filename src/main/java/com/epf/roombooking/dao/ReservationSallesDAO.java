package com.epf.roombooking.dao;

import com.epf.roombooking.entity.Reservation;
import com.epf.roombooking.entity.ReservationSalles;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ReservationSallesDAO extends JpaRepository<ReservationSalles,Long> {

    @Override
    List<ReservationSalles> findAll();

    ReservationSalles findByIdReservationSalles(Long id);
}
