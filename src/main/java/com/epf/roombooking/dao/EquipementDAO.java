package com.epf.roombooking.dao;

import com.epf.roombooking.entity.Equipements;
import com.epf.roombooking.entity.Salle;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface EquipementDAO extends JpaRepository<Equipements,Long> {
}
