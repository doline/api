package com.epf.roombooking.dao;

import com.epf.roombooking.entity.Product;
import org.springframework.data.repository.CrudRepository;

import org.springframework.stereotype.Repository;


import java.util.List;
import java.util.Optional;

@Repository

public interface ProductDAO extends CrudRepository<Product,Long> {

    List<Product> findAll();
    Optional<Product> findById(Long id);



}
