package com.epf.roombooking.dao;

import com.epf.roombooking.entity.Reservation;
import com.epf.roombooking.entity.ReservationUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ReservationUserDAO extends JpaRepository<ReservationUser,Long> {

    @Override
    List<ReservationUser> findAll();

    ReservationUser findByIdReservationUser(Long id);
}
