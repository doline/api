package com.epf.roombooking.dao;

import com.epf.roombooking.entity.Reservation;
import com.epf.roombooking.entity.Salle;
import com.epf.roombooking.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Date;
import java.util.List;

@Repository
public interface SalleDAO extends JpaRepository<Salle,Long> {

    Salle findByIdSalle(Long id);

    @Query("select s from Salle s " +
            "where s.capacite= :capaciteDemande " +
            "or s.capacite> :capaciteDemande")
    List<Salle> rechercherSallesAvecXPersonne(@Param("capaciteDemande") int capaciteDemande );

    @Query("select r from Salle s inner join ReservationSalles rs " +
            "on rs.salle.idSalle = s.idSalle inner join Reservation r " +
            "on r.idReservation = rs.reservation.idReservation where s.idSalle = :idSalle ")
    List<Reservation> reservationsDuneSalle(@Param("idSalle") long idSalle );

    @Query(value = "select salle.id_salle, salle.capacite, salle.nom_salle, salle.url_picture "+
            "from (select s.id_salle, capacite, nom_salle, url_picture " +
                "from salle s inner join reservation_salles rs on s.id_salle = rs.salle_id_salle inner join reservation r on rs.reservation_id_reservation = r.id_reservation " +
                "where r.date_reservation = :date and ((TIMESTAMP(r.heure_debut) <= TIMESTAMP ( :heureD ) and TIMESTAMP ( :heureD ) <= TIMESTAMP(r.heure_fin)) or (TIMESTAMP(r.heure_debut) <= TIMESTAMP ( :heureF ) and TIMESTAMP ( :heureF ) <= TIMESTAMP(r.heure_fin)) or (TIMESTAMP( :heureD ) <= TIMESTAMP ( r.heure_debut ) and TIMESTAMP ( r.heure_debut ) <= TIMESTAMP( :heureF )) or(TIMESTAMP( :heureD ) <= TIMESTAMP ( r.heure_fin ) and TIMESTAMP ( r.heure_fin ) <= TIMESTAMP( :heureF )) ) ) t "+
            "right outer join salle on t.id_salle = salle.id_salle where t.id_salle is null",nativeQuery = true)
    List<Salle> rechercherSallesDispo(@Param("date") LocalDate date, @Param("heureD") LocalDateTime heureD, @Param("heureF") LocalDateTime heureF);
//    Utilisation d'une requête SQL natif car un peu plus facile pour nous de le faire ainsi qu'en utilisant du code
}
