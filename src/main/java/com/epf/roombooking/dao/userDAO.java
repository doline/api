package com.epf.roombooking.dao;

import com.epf.roombooking.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

import java.sql.Date;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface userDAO extends JpaRepository<User,Long> {

    List<User> findAll();
    User findByIdUser(Long id);
    User findByEmail(String email);
    User findByEmailAndDateNaissance(String email, Date dateNaissance);


}
