package com.epf.roombooking.dao;

import com.epf.roombooking.entity.Reservation;
import com.epf.roombooking.entity.Salle;
import com.epf.roombooking.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ReservationDAO extends JpaRepository<Reservation, Long> {

    @Override
    List<Reservation> findAll();

    Reservation findByIdReservation(Long id);

    @Query("select u from User u inner join ReservationUser ru on u.idUser = ru.user.idUser inner join Reservation r on r.idReservation = ru.reservation.idReservation where r.idReservation = :idReservation ")
    List<User> MembresDuneReservation(@Param("idReservation") long idReservation );

    @Query("select r from Salle s inner join ReservationSalles rs " +
            "on rs.salle.idSalle = s.idSalle inner join Reservation r " +
            "on r.idReservation = rs.reservation.idReservation where s.idSalle = :idSalle and r.dateReservation >= current_date ")
    List<Reservation> reservationsDuneSalleAPartirDeMaintenant(@Param("idSalle") long idSalle );

    @Query("select r from Salle s inner join ReservationSalles rs " +
            "on rs.salle.idSalle = s.idSalle inner join Reservation r " +
            "on r.idReservation = rs.reservation.idReservation where r.dateReservation >= current_date ")
    List<Reservation> reservationsDesSallesAPartirDeMaintenant();
}
