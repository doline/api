package com.epf.roombooking;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RoomBookingApp {

    public static void main(String[] args) {
        SpringApplication.run(RoomBookingApp.class, args);
    }

}
