package com.epf.roombooking.entity;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;

@Data
@Entity
@Table(name = "equipements")
public class Equipements {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_EQUIPEMENT")
    @Getter
    @Setter
    private Long id_equipement;

    @Column(name = "NOM_EQUIPEMENT")
    @Getter
    @Setter
    private String nomEquipement ;


    @ManyToOne()
    private Salle salle;



}
