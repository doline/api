package com.epf.roombooking.entity;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;


@Entity
@Table(name = "products")
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter
    @Setter
    private Long id_product;

    @Getter
    @Setter
    private String designation;

    @Getter
    @Setter
    private double price;
}
