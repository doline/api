package com.epf.roombooking.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Table(name = "USER")
@ToString
@Entity
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_USER")
    @Getter
    @Setter
    private Long idUser;

    @Column(name = "NOM")
    @Getter
    @Setter
    private String nom;

    @Column(name = "EMAIL")
    @Getter
    @Setter
    private String email;

    @Column(name = "DATE_NAISSANCE")
    @Getter
    @Setter
    private Date dateNaissance;

    @Column(name = "IS_ADMIN")
    @Getter
    @Setter
    private boolean isAdmin;

    @OneToMany( mappedBy = "user")
    private List<ReservationUser> reservationUsers = new ArrayList<>() ;

}
