package com.epf.roombooking.entity;

public class Email {


    private String email;

    private String date;

    private String salle;

    private String nom;

    public Email() {
    }

    public Email(String email, String heure_debut, String salle, String nom) {
        this.email = email;
        this.date = heure_debut;
        this.salle = salle;
        this.nom = nom;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDate() {
        return date;
    }

    public void setHeure_debut(String heure_debut) {
        this.date = heure_debut;
    }

    public String getSalle() {
        return salle;
    }

    public void setSalle(String salle) {
        this.salle = salle;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }
}
