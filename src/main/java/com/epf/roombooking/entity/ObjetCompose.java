package com.epf.roombooking.entity;

import lombok.*;

import java.util.List;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class ObjetCompose {
    //Objet utilisé pour contenir un lot d'informations en une fois( Une reservation, la salle associée et les membres associés à cette réunion)
    private Reservation reservation;
    private Salle salle;
    private List<User> Membres;
}
