package com.epf.roombooking.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;


@Entity
@Getter
@Setter
@Table(name="salle")
@ToString
public class Salle {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_SALLE")
    private Long idSalle;

    @Column(name = "NOM_SALLE")
    private String nomSalle ;

    @Column(name = "URL_PICTURE")
    private String urlPicture;

    @Column(name = "CAPACITE")
    private Integer capacite;

    @OneToMany( mappedBy = "salle")
    private List<Equipements> equipements = new ArrayList<>();

    @OneToMany( mappedBy = "salle")
    @JsonIgnore
    private List<ReservationSalles> reservationSalles = new ArrayList<>() ;











}
