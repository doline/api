package com.epf.roombooking.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor
@ToString
@Table(name = "RESERVATION")
public class Reservation {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_RESERVATION")
    private Long idReservation;

    @Column(name = "DATE_RESERVATION")
    private LocalDate dateReservation ;

    @Column(name = "HEURE_DEBUT")
    private LocalDateTime heure_debut ;

    @Column(name = "HEURE_FIN")
    private LocalDateTime heurefin ;

    @OneToMany( mappedBy = "reservation")
    @JsonIgnore
    private List<ReservationSalles> reservationSalles = new ArrayList<>() ;

    @OneToMany( mappedBy = "reservation")
    @JsonIgnore
    private List<ReservationUser> reservationUsers = new ArrayList<>() ;

//    Les classes reservationUser et reservationSall servent d'intermediaire entre reservation et user; et reservation et salles respectivement.
//    Ceci est fait afin de ne pas avoir de Many To Many, mais plutôt plusieurs Many To One et One To Many


}
