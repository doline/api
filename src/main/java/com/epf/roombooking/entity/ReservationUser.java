package com.epf.roombooking.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Setter
@Getter
@NoArgsConstructor
@Table(name="reservation_users")
public class ReservationUser {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_RESERVATION_USER")
    private Long idReservationUser;

    @ManyToOne()
    private User user;

    @ManyToOne()
    private Reservation reservation;

    public ReservationUser(User user, Reservation reservation) {
        this.user = user;
        this.reservation = reservation;
    }
}
