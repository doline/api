package com.epf.roombooking.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name="reservation_salles")
public class ReservationSalles {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_RESERVATION_SALLES")
    private Long idReservationSalles;

    @ManyToOne()
    private Salle salle;

    @ManyToOne()
    private Reservation reservation;

    public ReservationSalles(Salle salle, Reservation reservation) {
        this.salle = salle;
        this.reservation = reservation;
    }
}
