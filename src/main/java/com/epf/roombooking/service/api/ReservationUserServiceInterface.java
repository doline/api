package com.epf.roombooking.service.api;

import com.epf.roombooking.entity.ReservationUser;

import java.util.List;
import java.util.Optional;

public interface ReservationUserServiceInterface {

    List<ReservationUser> listeReservationUser();
    ReservationUser ajouterReservationUser(ReservationUser reservationUser);
    ReservationUser modifierReservationUser(ReservationUser reservationUser);
    void supprimerReservationUser(long idReservationUser);
    ReservationUser rechercherReservationUserParId(long idReservationUser);
}
