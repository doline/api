package com.epf.roombooking.service.api;

import com.epf.roombooking.entity.Product;

import java.util.List;
import java.util.Optional;


public interface ProductServiceInterface {


    List<Product> getAllProducts();
    Optional<Product> finProductById(Long id);


}
