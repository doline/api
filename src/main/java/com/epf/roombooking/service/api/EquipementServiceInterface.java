package com.epf.roombooking.service.api;

import com.epf.roombooking.entity.Equipements;
import com.epf.roombooking.entity.Salle;

import java.util.List;
import java.util.Optional;

public interface EquipementServiceInterface {

    List<Equipements> getAll();
    Optional<Equipements> find(Long id);
    void delete(Long id) throws Exception;
    Equipements add(Equipements equipements);
    Equipements update(Equipements equipements);
}
