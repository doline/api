package com.epf.roombooking.service.api;

import com.epf.roombooking.entity.User;

import java.sql.Date;
import java.util.List;
import java.util.Optional;

public interface userServiceInterface {

    List<User> getUsers();

    User EnregistrerUser(User utilisateur);

    User ModifierUser(User utilisateur);

    void SupprimerUser(User utilisateur);

    User findUserById(Long id);
    User findUserByEmail(String email);
    User findUserByEmailAndDateNaissance(String email, Date dateNaissance);
}
