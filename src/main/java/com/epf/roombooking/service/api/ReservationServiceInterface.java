package com.epf.roombooking.service.api;

import com.epf.roombooking.entity.Reservation;
import com.epf.roombooking.entity.User;

import java.util.List;
import java.util.Optional;

public interface ReservationServiceInterface {

    List<Reservation> listeReservations();
    Reservation ajouterReservation(long idUser, long idSalle, Reservation reservation);
    Reservation modifierReservation(Reservation reservation);
    void supprimerReservation(long idReservation);
    Reservation rechercherReservationParId(long idReservation);
    List<User> membresDuneReservation(long idReservation);
    Reservation ajouterMembreAUneReunion(long idUser, long idReservation);
    List<Reservation> reservationsDuneSalleAPartirDeMaintenant(long idSalle);
    List<Reservation> reservationsDesSallesAPartirDeMaintenant();
}
