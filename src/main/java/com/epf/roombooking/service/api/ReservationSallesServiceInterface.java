package com.epf.roombooking.service.api;

import com.epf.roombooking.entity.ReservationSalles;

import java.util.List;
import java.util.Optional;

public interface ReservationSallesServiceInterface {

    List<ReservationSalles> listeReservationSalles();
    ReservationSalles ajouterReservationSalles(ReservationSalles reservationSalles);
    ReservationSalles modifierReservationSalles(ReservationSalles reservationSalles);
    void supprimerReservationSalles(long idReservationSalles);
    ReservationSalles rechercherReservationSalleParId(long idReservationSalle);
}
