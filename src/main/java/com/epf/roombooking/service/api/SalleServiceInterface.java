package com.epf.roombooking.service.api;


import com.epf.roombooking.entity.Salle;
import com.epf.roombooking.entity.Reservation;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;

public interface SalleServiceInterface {
    Salle rechercherSalleParId(long idReservation);

    List<Salle> getAll();
    Salle find(Long id);
    void delete(Long id) throws Exception;
    Salle add(Salle salle);
    Salle update(Salle salle);
    List<Salle> rechercherSallesAvecXPersonne(int capaciteDemande );

    List<Reservation> reservationsDuneSalle(long idSalle);

    List<Salle> rechercherSalleParDate(LocalDate dateDebut ,
                                             LocalDateTime heureDebut,
                                             LocalDateTime heureFin);

    AtomicBoolean checkIfRoomIsFree(LocalDate date, LocalDateTime heureDebut, LocalDateTime heureFin, long idSalle);
}
