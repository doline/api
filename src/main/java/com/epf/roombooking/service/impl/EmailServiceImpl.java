package com.epf.roombooking.service.impl;

import com.epf.roombooking.entity.Email;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

@Service
public class EmailServiceImpl {

    private JavaMailSender javaMailSender;

    public EmailServiceImpl(JavaMailSender javaMailSend) {
        this.javaMailSender = javaMailSend;
    }

    /**
     *
     * @param dto
     * @throws MailException
     */

    public void sendEmail(Email dto) throws MailException {

        SimpleMailMessage mail = new SimpleMailMessage();

        mail.setTo(dto.getEmail());
        mail.setSubject("INVITATION A LA REUNION ");
        mail.setText("M/Mme: " + dto.getNom() + "\n" +
                "vous etes invité a la reunion du " + dto.getDate() + "\n" +
                "salle " + dto.getSalle() + "\n" +
//                "Par  " + dto.getEmail() + "\n" +
                " merci pour votre confiance");

        javaMailSender.send(mail);
    }
}
