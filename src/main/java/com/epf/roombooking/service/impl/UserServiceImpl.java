package com.epf.roombooking.service.impl;

import com.epf.roombooking.dao.userDAO;
import com.epf.roombooking.entity.User;
import com.epf.roombooking.service.api.userServiceInterface;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements userServiceInterface {

    private final userDAO userDao;

    public UserServiceImpl(userDAO userDao) {
        this.userDao = userDao;
    }

    @Override
    public List<User> getUsers() {
        return userDao.findAll();
    }

    @Override
    public User EnregistrerUser(User utilisateur) {
        return userDao.save(utilisateur);
    }

    @Override
    public User ModifierUser(User utilisateur) {
        return userDao.save(utilisateur);
    }

    @Override
    public User findUserById(Long id) {
        return userDao.findByIdUser(id);
    }

    @Override
    public User findUserByEmail(String email) {
        return userDao.findByEmail(email);
    }

    @Override
    public void SupprimerUser(User utilisateur) {
        userDao.delete(utilisateur);
    }

    @Override
    public User findUserByEmailAndDateNaissance(String email, Date dateNaissance) {
        return userDao.findByEmailAndDateNaissance(email, dateNaissance);
    }
}
