package com.epf.roombooking.service.impl;

import com.epf.roombooking.dao.EquipementDAO;
import com.epf.roombooking.entity.Equipements;
import com.epf.roombooking.service.api.EquipementServiceInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class EquipementServiceImpl implements EquipementServiceInterface {

    @Autowired
    private EquipementDAO equipementDAO;

    @Override
    public List<Equipements> getAll() {
        return  equipementDAO.findAll();
    }

    @Override
    public Optional<Equipements> find(Long id) {
        return equipementDAO.findById(id);
    }

    @Override
    public void delete(Long id) throws Exception {
        equipementDAO.deleteById(id);
    }

    @Override
    public Equipements add(Equipements equipements) {
        equipementDAO.save(equipements);
        return equipements;
    }

    @Override
    public Equipements update(Equipements equipements) {
        equipementDAO.save(equipements);
        return equipements;
    }
}
