package com.epf.roombooking.service.impl;

import com.epf.roombooking.dao.ReservationSallesDAO;
import com.epf.roombooking.entity.ReservationSalles;
import com.epf.roombooking.service.api.ReservationSallesServiceInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ReservationSallesServiceImpl implements ReservationSallesServiceInterface {

    @Autowired
    private ReservationSallesDAO reservationSallesDAO;

    @Override
    public List<ReservationSalles> listeReservationSalles() {
        return reservationSallesDAO.findAll();
    }

    @Override
    public ReservationSalles ajouterReservationSalles(ReservationSalles reservationSalles) {
        return reservationSallesDAO.save(reservationSalles);
    }

    @Override
    public ReservationSalles modifierReservationSalles(ReservationSalles reservationSalles) {
        return reservationSallesDAO.save(reservationSalles);
    }

    @Override
    public void supprimerReservationSalles(long idReservationSalles) {
        reservationSallesDAO.deleteById(idReservationSalles);
    }

    @Override
    public ReservationSalles rechercherReservationSalleParId(long idReservationSalle) {
        return reservationSallesDAO.findByIdReservationSalles(idReservationSalle);
    }
}
