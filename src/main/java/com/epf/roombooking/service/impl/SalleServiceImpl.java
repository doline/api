package com.epf.roombooking.service.impl;

import com.epf.roombooking.dao.SalleDAO;

import com.epf.roombooking.entity.Reservation;
import com.epf.roombooking.entity.Salle;
import com.epf.roombooking.service.api.SalleServiceInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;

@Service
public class SalleServiceImpl implements SalleServiceInterface {

    @Autowired
    private SalleDAO salleDAO;

    @Override
    public List<Salle> getAll() {
        return salleDAO.findAll();
    }

    @Override
    public Salle find(Long id) {
        return salleDAO.findByIdSalle(id);
    }

    @Override
    public void delete(Long id) throws Exception {
            salleDAO.deleteById(id);
    }

    @Override
    public Salle add(Salle salle) {
        salleDAO.save(salle);
        return salle;
    }

    @Override
    public Salle update(Salle salle) {
        salleDAO.save(salle);
        return salle;
    }
    @Override
    public Salle rechercherSalleParId(long idSalle) {
        return salleDAO.findByIdSalle(idSalle);
    }

    @Override
    public List<Salle> rechercherSallesAvecXPersonne(int capaciteDemande) {
        return salleDAO.rechercherSallesAvecXPersonne(capaciteDemande);
    }

    @Override
    public List<Reservation> reservationsDuneSalle(long idSalle) {
        return salleDAO.reservationsDuneSalle(idSalle);
    }

    @Override
    public List<Salle> rechercherSalleParDate(LocalDate dateDebut,
                                                    LocalDateTime heureDebut,
                                                    LocalDateTime heureFin){
       return  salleDAO.rechercherSallesDispo(dateDebut, heureDebut, heureFin);
    }

    @Override
    public AtomicBoolean checkIfRoomIsFree(LocalDate date,LocalDateTime heureDebut, LocalDateTime heureFin, long idSalle){
       /* Verifie si une salle donnée est libre. Commence par obtenir la liste des salles libres. Ensuite,
       si la salle recherché est dans la liste des Salles libres, on retourne vrai. Si elle n'y est pas on retourne faux. */
        List<Salle> salles = salleDAO.rechercherSallesDispo(date, heureDebut, heureFin);
        AtomicBoolean found = new AtomicBoolean(false);
        salles.forEach(salle -> {
            if (salle.getIdSalle()==idSalle){
                found.set(true);
            }
        });
        return found;
    }


}
