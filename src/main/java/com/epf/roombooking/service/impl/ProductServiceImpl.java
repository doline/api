package com.epf.roombooking.service.impl;

import com.epf.roombooking.dao.ProductDAO;
import com.epf.roombooking.entity.Product;

import com.epf.roombooking.service.api.ProductServiceInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductServiceImpl implements ProductServiceInterface {


    @Autowired
    ProductDAO productDAO;


    @Override
    public List<Product> getAllProducts() {
        return productDAO.findAll();
    }

    @Override
    public Optional<Product> finProductById(Long id) {
        return productDAO.findById(id);
    }


}
