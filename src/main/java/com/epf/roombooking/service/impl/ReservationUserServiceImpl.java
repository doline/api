package com.epf.roombooking.service.impl;

import com.epf.roombooking.dao.ReservationUserDAO;
import com.epf.roombooking.entity.ReservationUser;
import com.epf.roombooking.service.api.ReservationUserServiceInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ReservationUserServiceImpl implements ReservationUserServiceInterface {

    @Autowired
    private ReservationUserDAO reservationUserDAO;

    @Override
    public List<ReservationUser> listeReservationUser() {
        return reservationUserDAO.findAll();
    }

    @Override
    public ReservationUser ajouterReservationUser(ReservationUser reservationUser) {
        return reservationUserDAO.save(reservationUser);
    }

    @Override
    public ReservationUser modifierReservationUser(ReservationUser reservationUser) {
        return reservationUserDAO.save(reservationUser);
    }

    @Override
    public void supprimerReservationUser(long idReservationUser) {
        reservationUserDAO.deleteById(idReservationUser);
    }

    @Override
    public ReservationUser rechercherReservationUserParId(long idReservationUser) {
        return reservationUserDAO.findByIdReservationUser(idReservationUser);
    }
}
