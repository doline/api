package com.epf.roombooking.service.impl;

import com.epf.roombooking.dao.ReservationDAO;
import com.epf.roombooking.entity.*;
import com.epf.roombooking.service.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ReservationServiceImpl implements ReservationServiceInterface {

    @Autowired
    private ReservationDAO reservationDAO;
    @Autowired
    private userServiceInterface userServiceInterface;
    @Autowired
    private SalleServiceInterface salleServiceInterface;
    @Autowired
    private ReservationUserServiceInterface reservationUserServiceInterface;
    @Autowired
    private ReservationSallesServiceInterface reservationSallesServiceInterface;
    @Autowired
    private EmailServiceImpl emailService;
    @Override
    public List<Reservation> listeReservations() {
        return reservationDAO.findAll();
    }

    @Override
    public Reservation ajouterReservation(long idUser, long idSalle, Reservation reservation) {
        /* On récupères le user qui a crée la reservation et la salle demandé,
         On crée ensuite une réservation,et on la relie au user trouvé et à la salle demandé.*/
        User user =userServiceInterface.findUserById(idUser);
        Salle salle = salleServiceInterface.rechercherSalleParId(idSalle);
        reservationDAO.save(reservation);
        ReservationUser reservationUser = new ReservationUser(user,reservation);
        ReservationSalles reservationSalles =new ReservationSalles(salle,reservation);
        reservationUserServiceInterface.ajouterReservationUser(reservationUser);
        reservationSallesServiceInterface.ajouterReservationSalles(reservationSalles);
        return reservation;
    }

    @Override
    public Reservation modifierReservation(Reservation reservation) {
        return reservationDAO.save(reservation);
    }

    @Override
    public void supprimerReservation(long idReservation) {
        reservationDAO.deleteById(idReservation);
    }

    @Override
    public Reservation rechercherReservationParId(long idReservation) {
        return reservationDAO.findByIdReservation(idReservation);
    }

    @Override
    public List<User> membresDuneReservation(long idReservation) {
        return reservationDAO.MembresDuneReservation(idReservation);
    }

    @Override
    public Reservation ajouterMembreAUneReunion(long idUser, long idReservation) {
        /*
        On recherche le membre invité, on le relie à la réunion, grace à l'objet ReservationUser qui sert d'intermediaire entre reservation et user,
        ensuite on lui envoi un mail pour lui signifier qu'il a été ajouté à une réunion
        */
        User user =userServiceInterface.findUserById(idUser);
        Reservation reservation = reservationDAO.findByIdReservation(idReservation);
        ReservationUser reservationUser = new ReservationUser(user,reservation);
        reservationUserServiceInterface.ajouterReservationUser(reservationUser);
        emailService.sendEmail(new Email(user.getEmail(), reservation.getDateReservation().toString(),reservation.getReservationSalles().get(0).getSalle().getNomSalle(), user.getNom()));
        return reservation;
    }

    @Override
    public List<Reservation> reservationsDuneSalleAPartirDeMaintenant(long idSalle) {
        return reservationDAO.reservationsDuneSalleAPartirDeMaintenant(idSalle);
    }

    @Override
    public List<Reservation> reservationsDesSallesAPartirDeMaintenant() {
        return reservationDAO.reservationsDesSallesAPartirDeMaintenant();
    }
}
