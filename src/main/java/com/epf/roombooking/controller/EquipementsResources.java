package com.epf.roombooking.controller;

import com.epf.roombooking.entity.Equipements;
import com.epf.roombooking.service.api.EquipementServiceInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("api/v1/eq")
public class EquipementsResources {

    @Autowired
    EquipementServiceInterface equipementServiceInterface;

    @GetMapping(value = "/home")
    public String helloWorld()
    {
        return "Hello home ";

    }

    @GetMapping
    public List<Equipements> getAll()
    {
        return equipementServiceInterface.getAll();

    }

    @GetMapping("/{equipement_id}")
    private Optional<Equipements> getById(@PathVariable("equipement_id") Long equipement_id)
    {
        return  equipementServiceInterface.find(equipement_id);
    }


    @DeleteMapping("/{equipement_id}")
    private void delete(@PathVariable("equipement_id") Long equipement_id) throws Exception {
        equipementServiceInterface.delete(equipement_id);
        ResponseEntity
                .status(HttpStatus.CREATED)
                .body("equipement with ID" + equipement_id +"\t" +
                        " has been deleted with succeseffuly");
    }



    @PostMapping("/equipements")
    private Equipements save(@RequestBody Equipements equipements)
    {
        equipementServiceInterface.add(equipements);
        return equipements;
    }

    @PutMapping("/equipements")
    private Equipements update(@RequestBody Equipements equipements)
    {
        equipementServiceInterface.update(equipements);
        return equipements;
    }
}
