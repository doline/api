package com.epf.roombooking.controller;



import com.epf.roombooking.entity.Email;
import com.epf.roombooking.entity.Reservation;
import com.epf.roombooking.entity.Salle;
import com.epf.roombooking.service.api.ProductServiceInterface;
import com.epf.roombooking.service.api.ReservationServiceInterface;
import com.epf.roombooking.service.impl.EmailServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController

@CrossOrigin
@RequestMapping("/api/products")
public class ProductResources {

//    Une classe pour faire divers tests

    @Autowired
    private EmailServiceImpl emailService;

    @Autowired
    ProductServiceInterface productServiceInterface;
    @Autowired
    ReservationServiceInterface reservationServiceInterface;

    @GetMapping(value = "/hello")
    public ResponseEntity helloWorld() throws Exception {

        return ResponseEntity.status(HttpStatus.CREATED).body("HelloWorld");

    }


    @PostMapping("/inscription")
    public String InscrirerInstitution(@RequestBody Email email){

        emailService.sendEmail(email);
        return "email bien envoye";

    }




    @GetMapping
    public ResponseEntity doGet() throws Exception {

        return ResponseEntity.status(HttpStatus.CREATED).body(productServiceInterface.getAllProducts());

    }

    @DeleteMapping("/product/{product_id}")
    private void deleteProduct(@PathVariable("product_id") Long product_id)
    {
        productServiceInterface.finProductById(product_id);
    }


    @GetMapping("/test")
    public ResponseEntity<String> FabTests(){
        Reservation reservation = reservationServiceInterface.rechercherReservationParId(10L);

        System.out.println(reservation.getReservationSalles().get(0).getSalle());
        return new ResponseEntity<>("Bien Joué",HttpStatus.OK);

    }

}
