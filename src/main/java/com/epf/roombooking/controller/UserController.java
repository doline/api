package com.epf.roombooking.controller;

import com.epf.roombooking.entity.Email;
import com.epf.roombooking.entity.User;
import com.epf.roombooking.service.api.userServiceInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

@RestController
@CrossOrigin
@RequestMapping("/api/user")
public class UserController {

    @Autowired
    private userServiceInterface userService;

    @PostMapping("/inscriptionUser")
    public User InscrirerInstitution(@RequestBody User user){
        System.out.println(user.isAdmin());
        userService.EnregistrerUser(user);
        return user;

    }

    @GetMapping("/listUser")
    public List<User> ListAdmin(){
        return userService.getUsers();
    }

    @PutMapping("/updateUser")
    public User UpdateAdmin(@RequestBody User userbaal){
        return userService.ModifierUser(userbaal);
    }

    @DeleteMapping("/delete/{id}")
    public void deleteAdmin(@PathVariable Long id){
        User user = userService.findUserById(id);
        userService.SupprimerUser(user);
    }

    @GetMapping("/{email}")
    public User findUserByEmail(@PathVariable String email){
        return userService.findUserByEmail(email);
    }

    @GetMapping("login/{email}/{dateNaissance}")
    public ResponseEntity<User> findUserByEmailAndDateNaissance(@PathVariable String email, @PathVariable String dateNaissance){
       User user= userService.findUserByEmailAndDateNaissance(email,  Date.valueOf(dateNaissance));
        if (user == null || user.isAdmin()==false)
        {
            return new ResponseEntity<>(null, HttpStatus.FORBIDDEN);
        }
        else {
            return new ResponseEntity<>(user, HttpStatus.OK);
        }
    }
}
