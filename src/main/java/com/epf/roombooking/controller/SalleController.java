package com.epf.roombooking.controller;

import com.epf.roombooking.entity.Equipements;
import com.epf.roombooking.entity.ObjetCompose;
import com.epf.roombooking.entity.Reservation;
import com.epf.roombooking.entity.Salle;

import com.epf.roombooking.service.api.ReservationServiceInterface;
import com.epf.roombooking.service.api.SalleServiceInterface;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;


@RestController
@CrossOrigin
@RequestMapping("api/v1/room")
public class SalleController {


    @Autowired
    SalleServiceInterface salleServiceInterface;
    @Autowired
    ReservationServiceInterface reservationServiceInterface;

    @GetMapping(value = "/home")
    public String helloWorld()
    {
        return "Hello home ";

    }

    @GetMapping
    public List<Salle> getAll()
    {
        return salleServiceInterface.getAll();

    }

    @GetMapping("/{salle_id}")
    private Salle getById(@PathVariable("salle_id") Long salle_id)
    {
        return  salleServiceInterface.find(salle_id);
    }


    @DeleteMapping("/{salle_id}")
    private void delete(@PathVariable("salle_id") Long salle_id) throws Exception {
        salleServiceInterface.delete(salle_id);
        ResponseEntity
                .status(HttpStatus.OK)
                .body("Room with ID" + salle_id +"\t" +
                " has been deleted successfully");
    }

    @PostMapping("/salle")
    private Salle save(@RequestBody Salle salle)
    {
        salleServiceInterface.add(salle);
        return salle;
    }

    @PutMapping("/salle")
    private Salle update(@RequestBody Salle salle)
    {
        salleServiceInterface.update(salle);
        return salle;
    }

    @GetMapping("/rechercherSallesDispo/{capaciteDemande}")
    public List<Salle> reservationSalleParCapacity(@PathVariable int capaciteDemande){
        return salleServiceInterface.rechercherSallesAvecXPersonne(capaciteDemande);
    }

    @GetMapping("/disponibiliteDuneSalle/{idSalle}")
    public List<Reservation> reservationSalleParId(@PathVariable long idSalle){
//        Retourne toutes les reservations d'une salle d'après son id
        return salleServiceInterface.reservationsDuneSalle(idSalle);
    }

    @GetMapping("/getSalleInfo/{idSalle}")
    public ResponseEntity<Salle> obtenirToutesLesInfoDuneSalles(@PathVariable long idSalle){
        Salle salle= salleServiceInterface.rechercherSalleParId(idSalle);
        System.out.println(reservationServiceInterface.reservationsDuneSalleAPartirDeMaintenant(idSalle));
        return new ResponseEntity<>(salle,HttpStatus.OK);
    }

    @GetMapping("/equipementsDuneSalle/{idSalle}")
    public ResponseEntity<List<Equipements>> obtenirLesEquipementsDuneSalles(@PathVariable long idSalle){
        Salle salle= salleServiceInterface.rechercherSalleParId(idSalle);
        return new ResponseEntity<>(salle.getEquipements(),HttpStatus.OK);
    }

    @GetMapping("/toutesLesReservationsFutures")
    public ResponseEntity<List<ObjetCompose>> obtenirLesReservationsFuturs(){
//        Retourne toutes les reservations à partir de la date et de l'heure actuelle
        List<ObjetCompose> objetComposes = new ArrayList<>();
        List<Reservation> reservations= reservationServiceInterface.reservationsDesSallesAPartirDeMaintenant();
        reservations.forEach(reservation -> {
            objetComposes.add(
                    new ObjetCompose(
                            reservation,
                            reservation.getReservationSalles().get(0).getSalle(),
                            reservationServiceInterface.membresDuneReservation(reservation.getIdReservation())
                    )
            );
        });
        return new ResponseEntity<>(objetComposes,HttpStatus.OK);
    }
    @GetMapping("/dispo/{dateDebut}/{dateFin}/{heureDebut}/{heureFin}")
    public List<Salle> reservationSalleParDate(
            @PathVariable String dateDebut
            ,@PathVariable  String dateFin,
            @PathVariable String heureDebut,
            @PathVariable String heureFin
    ){
//        Changer les dates et les heures de debut et de fin d'une réservation en type convenables pour travailler avec dans notre Base de données
        LocalDate dateD=LocalDate.parse(dateDebut); // Convertir la date
        LocalDateTime heureD= LocalDateTime.parse(dateDebut+"T"+heureDebut); //Convertir l'heure de début en un format convenable
        LocalDateTime heureF= LocalDateTime.parse(dateFin+"T"+heureFin); //Convertir l'heure de fin en un format convenable
        return salleServiceInterface.rechercherSalleParDate(dateD,heureD,heureF);
    }

    @GetMapping("/checkIfRoomDispo/{dateDebut}/{heureDebut}/{heureFin}/{idSalle}")
    public ResponseEntity<AtomicBoolean> checkIfRoomIsFree(@PathVariable String dateDebut, @PathVariable String heureDebut, @PathVariable String heureFin, @PathVariable long idSalle){
//        Verifier si une salle précise est dispo à une date et une plage horaire précise. Retourne True si elle est dispo et False sinon
        AtomicBoolean found = salleServiceInterface.checkIfRoomIsFree(LocalDate.parse(dateDebut), LocalDateTime.parse(dateDebut+"T"+heureDebut),LocalDateTime.parse(dateDebut+"T"+heureFin), idSalle);
        return new ResponseEntity<>(found,HttpStatus.OK);
    }
}

