package com.epf.roombooking.controller;

import com.epf.roombooking.entity.*;
import com.epf.roombooking.service.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@CrossOrigin
@RestController
@RequestMapping("/api/reservation")
public class ReservationController {

    @Autowired
    private ReservationServiceInterface reservationServiceInterface;


    @GetMapping(value = "/")
    public List<Reservation> listeReservations(){
        return reservationServiceInterface.listeReservations();
    }
    @PostMapping(value = "/{idUser}/{idSalle}")
    public ResponseEntity<Reservation> creerReservation(@PathVariable long idUser, @PathVariable long idSalle,@RequestBody Reservation reservation){
//        On utilise le ResponseEntity pour retourner le code approprié (201 et non 200)
        Reservation new_reservation = reservationServiceInterface.ajouterReservation(idUser, idSalle, reservation);
        return new ResponseEntity<>(new_reservation,HttpStatus.CREATED);
    }

    @GetMapping(value = "/{idReservation}")
    public ResponseEntity<Reservation> rechercherUneReservationParSonId(@PathVariable long idReservation){
        return new ResponseEntity<>(reservationServiceInterface.rechercherReservationParId(idReservation),HttpStatus.OK);
    }

    @DeleteMapping(value = "/{idReservation}")
    public ResponseEntity<Reservation> supprimerUneReservation(@PathVariable long idReservation){
        reservationServiceInterface.supprimerReservation(idReservation);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PutMapping(value = "/{idReservation}")
    public Reservation modifierUneReservation(@PathVariable long idReservation,@RequestBody Reservation reservationAModifier){
        return reservationServiceInterface.modifierReservation(reservationAModifier);
    }

    @GetMapping(value = "/membresDuneReunion/{idReservation}")
    public List<User> MembresDuneReservation(@PathVariable long idReservation){
        return reservationServiceInterface.membresDuneReservation(idReservation);
    }

    @GetMapping(value = "/ajouterMembre/{idUser}/{idReservation}")
    public ResponseEntity<Reservation> ajouterMembreAUneReunion(@PathVariable long idUser, @PathVariable long idReservation){
        return new ResponseEntity<>(reservationServiceInterface.ajouterMembreAUneReunion(idUser, idReservation),HttpStatus.ACCEPTED);
    }
}
